#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    void setBoard(Board *board) {b = board;}
private:
    std::vector<Move*> getLegalMoves();
    Move *working();
    Move *simple();
    Move *minimax();
    int alphabeta(Board *board, int depth, int alpha, int beta, bool maximizingPlayer, bool root, int n_pieces);
    Side s;
    Side os;
    Board *b;
    int n_moves;
    
};

#endif
