#include "player.h"

// Linus has committed.
// Jeff has edited this file

// 1 for a working AI; 2 to beat simple player
// 3 to use minimax; 4 to use alpha beta pruning
#define VERSION 4
#define DEPTH 5



/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    n_moves = 0;

    s = side;
    if (side == WHITE)
    {
      os = BLACK;
    }
    else
    {
      os = WHITE;
    }
    b = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete b;
}



/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    // process opponent's move
    b->doMove(opponentsMove, os);

    // calculate your best move
    Move *move = NULL;
    switch (VERSION) {
        case 1:
            move = working();
            break;
        case 2:
            move = simple();
            break;
        case 3:
            move = minimax();
            break;
        case 4:
            // our best AI

            // start the alpha beta pruning algorithm
            int index = alphabeta(b, DEPTH, -10000, 10000, true, true, 2 * n_moves);
            vector<Move*> legalMoves = b->getLegalMoves(s);
            if (legalMoves.size() > 0) {
                move = legalMoves[index];
            }

            // clean memory
            vector<Move*>::iterator it;
            for(it = legalMoves.begin(); it != legalMoves.end(); it++) {
                if (*it == move) {continue;}
                delete *it;
            }
            break;
    }
       
    // perform the move you chose
    b->doMove(move, s);
    n_moves++;
    return move;
}


/*
 * Returns a move that results in a working AI. Specifically, the first
 * valid move it comes across when searching the board.
 */
Move *Player::working() {
    for (int i = 0; i < 8; i++) {  // loop over columns
        for (int j = 0; j < 8; j++) {  // loop over rows
            Move *move = new Move(i, j);
            if (b->checkMove(move, s)) {
                return move;
            }
            else {
                delete move;
            }
        }
    }
    return NULL;
}


/*
 * Returns the valid move that results in the highest heuristic after the
 * single move. Should beat SimplePlayer
 */
Move *Player::simple() {
    Move *bestMove;
    int score, bestScore = -36; // worst possible score given our heuristic
    // get valid moves
    std::vector<Move*> legalMoves = b->getLegalMoves(s);
    std::vector<Move*>::iterator iter;

    // no legal moves avaliable
    if (legalMoves.size() == 0)
    {
      return NULL;
    }

    for (iter = legalMoves.begin(); iter != legalMoves.end(); iter++)
    {
        // make a copy of the board
        Board* cb = b->copy();
        // make the move
        cb->doMove(*iter, s);
        // compute the score
        score = cb->doHeuristic(s, testingMinimax, 0);
        if (score >= bestScore)
        {
            bestScore = score;
            bestMove = *iter;
        }
        delete cb;
    }

    // allocate bestMove its own memory...
    bestMove = new Move(bestMove->getX(), bestMove->getY());
    // ... and clear out legalMoves
    while (legalMoves.size() != 0)
    {
        // delete the allocate move
        delete legalMoves.back();
        // and remove the pointer from the vector
        legalMoves.pop_back();
    }

    return bestMove;
}

/*
 * Returns the valid move that leads to the maximum possible minimum heuristic
 * after a specified number of rounds.
 */
Move *Player::minimax() {
    int rounds = 1;
    int depth = 2 * rounds;
    if (testingMinimax) {
        depth = 2;
    }

    Move *bestMove = NULL;

    vector<Move*> legalMoves = b->getLegalMoves(s);

    int maxScore = -1000;
    vector<Move*>::iterator it;
    for (it = legalMoves.begin(); it != legalMoves.end(); it++) {  // for each valid move
        // create a board with this move
        Board *board = b->copy();
        board->doMove(*it, s);
        
        // find the board with the highest score
        int score = board->getMinScore(s, depth, testingMinimax);
        if (score > maxScore) {
            maxScore = score;
            bestMove = *it;
        }
    }
    return bestMove;
}


/* Return the move best possible move after search to depth-ply using
 * alpha beta pruning
 * Based on the pseudocode on Wikipedia's alpha beta pruning page
 */
int Player::alphabeta(Board *board, int depth, int alpha, int beta, bool maximizingPlayer, bool root, int n_pieces) {
    // base case
    if (depth == 0 || board->isDone() ) {
        return board->doHeuristic(s, n_pieces, false);
    }

    // recursive step
    if (maximizingPlayer) {
        vector<Move*> legalMoves = board->getLegalMoves(s);
        vector<Move*>::iterator it;
        int index = 0;
        for (it = legalMoves.begin(); it != legalMoves.end(); it++) { // loop over valid moves
            int temp = alpha;
            Board *b = board->copy();
            b->doMove(*it, s);
            alpha = max(alpha, alphabeta(b, depth - 1, alpha, beta, true, false, n_pieces + 1));
            delete b;
            if (beta <= alpha) {break;}

            // save the index if this is the best move to make
            if (alpha - temp != 0) { // if this move increased alpha
                index = it - legalMoves.begin();
            }
        }

        // clean memory
        for (it = legalMoves.begin(); it != legalMoves.end(); it++) {
            delete *it;
        }

        // Usually return alpha. Except the root node which returns the index of the best move
        if (root) {return index;} 
        return alpha;
    }

    else {  // not maximizingPlayer
        vector<Move*> legalMoves = board->getLegalMoves(os);
        vector<Move*>::iterator it;
        for (it = legalMoves.begin(); it != legalMoves.end(); it++) { // loop over valid moves
            Board *b = board->copy();
            b->doMove(*it, s);
            beta = min(beta, alphabeta(b, depth - 1, alpha, beta, true, false, n_pieces + 1));
            delete b;

            if (beta <= alpha) {return beta;};
        }

        // clean memory
        for (it = legalMoves.begin(); it != legalMoves.end(); it++) {
            delete *it;
        }

        return beta;
    }
}



