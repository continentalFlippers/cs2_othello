#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

/*
 * Returns an int representing the heuristic for the given board. 
 * s is the side to calculate the heuristic for.
 * os is the other side.
 * testingMinimax will use a simpler heuristic for testing purposes
 */
int Board::doHeuristic(Side s, bool testingMinimax, int n_pieces)
{

  int score = 0;
  Side os = (s == BLACK) ? WHITE : BLACK;
  double n_moves_coeffecient = .3;
 
  bool permtable [64] = { false };
  // give heaviest weighing to permanent members
  computePermtable(permtable);
  for (int i = 0; i < 8; i++)
      {
        for (int j = 0; j < 8; j++)
        {
          if (get(s, i, j))
          {
            // permanent members
            if (*(permtable + 8 * i + j))
            {
              score += 20;
            }
            // diag adjacent to corners
            else if (((i == 1 || i == 6) && (j == 1 || j == 6)))
            {
              score -= 2;
            }
            // other adjacent to corners
            else if (((i == 0 || i == 7) && (j == 1 || j == 6)) ||\
                   ((i == 1 || i == 6) && (j == 0 || j == 7)))
            {
              score -= 2;
            }
            // sides
            else if (i == 0 || i == 7 || j == 0 || j == 7)
            {
              score += 5;
            }
            // everything else
            else
            {
              score += 1;
            }
          }
          else if (get(os, i, j))
          {
            // permanent members
            if (*(permtable + 8 * i + j))
            {
              score -= 20;
            }
            // diag adjacent to corners
            else if (((i == 1 || i == 6) && (j == 1 || j == 6)))
            {
              score += 2;
            }
            // other adjacent to corners
            else if (((i == 0 || i == 7) && (j == 1 || j == 6)) ||\
                   ((i == 1 || i == 6) && (j == 0 || j == 7)))
            {
              score += 2;
            }
            // sides
            else if (i == 0 || i == 7 || j == 0 || j == 7)
            {
              score -= 5;
            }
            // everything else
            else
            {
              score -= 1;
            }
          } 
        }
      }
   
    // increase heuristic based on number of available moves
    int n_moves = this->get_n_moves(s);
    double final = (1 - n_moves_coeffecient) * score + n_moves_coeffecient *    n_moves;
 
    return (int) final;
}

/*
 * Incomplete function that fills in true for each member of the board that
 * cannot be flipped; as of now, only the edges are checked.
 */
void Board::computePermtable(bool *permtable)
{
  if(occupied(0, 0))
  {
    *(permtable + 8 * 0 + 0) = true;
  }    
  if(occupied(0, 7))
  {
    *(permtable + 8 * 0 + 7) = true;
  }    
  if(occupied(7, 0))
  {
    *(permtable + 8 * 7 + 0) = true;
  }    
  if(occupied(7, 7))
  {
    *(permtable + 8 * 7 + 7) = true;
  }    
  
  int i, j;

  i = 0;
  j = 0;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i + 1, j) && get(BLACK, i + 1 , j) == get(BLACK, i, j))
    {
      *(permtable + 8 * (i + 1) + j) = true;
    }
    i++;
  }

  i = 7;
  j = 0;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i - 1, j) && get(BLACK, i - 1 , j) == get(BLACK, i, j))
    {
      *(permtable + 8 * (i - 1) + j) = true;
    }
    i--;
  }

  i = 0;
  j = 7;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i + 1, j) && get(BLACK, i + 1 , j) == get(BLACK, i, j))
    {
      *(permtable + 8 * (i + 1) + j) = true;
    }
    i++;
  }

  i = 7;
  j = 7;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i - 1, j) && get(BLACK, i - 1 , j) == get(BLACK, i, j))
    {
      *(permtable + 8 * (i - 1) + j) = true;
    }
    i--;
  }


  i = 0;
  j = 0;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i, j + 1) && get(BLACK, i , j + 1) == get(BLACK, i, j))
    {
      *(permtable + 8 * i + (j + 1)) = true;
    }
    j++;
  }

  i = 0;
  j = 7;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i, j - 1) && get(BLACK, i , j - 1) == get(BLACK, i, j))
    {
      *(permtable + 8 * i + (j - 1)) = true;
    }
    j++;
  }

  i = 7;
  j = 0;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i, j + 1) && get(BLACK, i , j + 1) == get(BLACK, i, j))
    {
      *(permtable + 8 * i + (j + 1)) = true;
    }
    j++;
  }

  i = 7;
  j = 7;
  while (onBoard(i, j) && *(permtable + 8 * i + j))
  {
    if (occupied(i, j - 1) && get(BLACK, i , j - 1) == get(BLACK, i, j))
    {
      *(permtable + 8 * i + (j - 1)) = true;
    }
    j++;
  }
}



/*
 * Return the number of valid moves player side can make.
 */
int Board::get_n_moves(Side side)
{
    vector<Move*> legalMoves = this->getLegalMoves(side);
    int n_moves = legalMoves.size();

    // clean up memory
    vector<Move*>::iterator it;
    for(it = legalMoves.begin(); it != legalMoves.end(); it++) {
        delete *it;
    }
    return n_moves;
}

/* 
 *Return the legal moves player s can make.
 */
std::vector<Move*> Board::getLegalMoves(Side s)
{
    std::vector<Move*> legal;
   
    // iterate thru all spots and return legal moves
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Move* m = new Move(i, j); 
            if (checkMove(m, s))
            {
              legal.push_back(m);
            }
            else
            {
                delete m;
            }
        }
    } 
    return legal;
}

 /*
  * Returns an int representing the minimum heuristic after trying
  * every valid move down to 2ply depth.
  * testingMinimax uses the simpler heuristic for testing purposes
  */
int Board::getMinScore(Side s, int depth, bool testingMinimax)
{
    // assume the other player goes next
    int minScore = 1000000;
    Side other = (s == BLACK) ? WHITE : BLACK;
    vector<Move*> legalMoves = getLegalMoves(other);
    vector<Move*>::iterator it;
    for (it = legalMoves.begin(); it != legalMoves.end(); it++) {  // for each valid move
        // create a board with this move
        Board *board = this->copy();
        board->doMove(*it, other);

        // find the board with the lowest score
        int score = board->doHeuristic(s, testingMinimax, 0);
        if (score < minScore) {
            minScore = score;
        }
    }
    return minScore;
}
