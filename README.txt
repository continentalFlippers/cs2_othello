What each group member contributed:
Linus and Jeff contributed equally. In the first week we started by discussing
how to break the assignment into parts that be written individually and a framework
that would combine our work. We outlined the general algorithms together and made 
a list of functions to write along with their arguments and return values. Linus
wrote the functions to calculate the heuristic and beat simplePlayer. Jeff wrote
the functions to make a make a working AI and incorporate minimax.

In the second week we talked about what classes to make and data structures to use
in order to implement alpha beta pruning. We ended up using an algorithm based on
the pseudocode on Wikipedia's alpha-beta pruning article. Both of us coded the
algorithm.






Improvements and strategy.
After creating a functioning AI last week, we made many improvements to make it
tournament-worthy. We implemented the required improvements last week--using a
heuristic to estimate which move would be best and using the minimax algorithm
to consider the long-term payoff of a move as well. This week we incorporated an
alpha-beta pruning algorithm to reduce processing time allowing us to look further
into the future. By default, our AI create and search a 5-ply depth decision tree.
We made additional improvements by making adjustments to our heuristic calculation
and empirically optimizing it. These include favoring moves that give us many
possible moves, and changing the weights of corners, and spots adjacent to corners.

Our strategy should be succesful in the tournament because we evaluate moves to a 
depth of 5 and estimate their success with a well-chosen heuristic. This is deeper
than most benchmark AI's.

Furthermore, the heuristic has been modified so that the AI will know when certain
edge pieces are adjacent to corner pieces, and thus cannot be flipped anymore. 
Our heuristic is highly aggressive to grabbing onto the edges, and if successful at
grabbing at least one edge early in the game, our AI tends to win.
